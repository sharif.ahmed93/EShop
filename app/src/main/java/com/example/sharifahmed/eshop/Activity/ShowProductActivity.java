package com.example.sharifahmed.eshop.Activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sharifahmed.eshop.Adapter.CustomAdapter;
import com.example.sharifahmed.eshop.Database.ShopDatabaseOperation;
import com.example.sharifahmed.eshop.Model.ShopModel;
import com.example.sharifahmed.eshop.R;

import java.io.Serializable;
import java.util.ArrayList;

public class ShowProductActivity extends AppCompatActivity {


    TextView noneProduct;
    TextView goShoProduct;
    ListView showlist;
    ShopDatabaseOperation shopDatabaseOperation;
    ArrayList<ShopModel>productslist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_product);



        shopDatabaseOperation=new ShopDatabaseOperation(this);
        showlist = (ListView) findViewById(R.id.showproductListId);

        noneProduct = (TextView) findViewById(R.id.noneProductTextId);
        goShoProduct = (TextView) findViewById(R.id.goShowProductTextId);

        productslist=shopDatabaseOperation.getAllProduct();



        if(productslist.size()!=0) {
            CustomAdapter adapter = new CustomAdapter(this, productslist);
            showlist.setAdapter(adapter);
        }
        else{
//            Toast.makeText(getApplicationContext(),"there Is No data",Toast.LENGTH_SHORT).show();
//            Intent goinsertIntent = new Intent(getApplicationContext(),ProductInsertActivity.class);
//            startActivity(goinsertIntent);
//            finish();
            noneProduct.setText("There Is No data To be display");
            goShoProduct.setText("Please Click Here To Insert Product");
            goShoProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent goinsertIntent = new Intent(getApplicationContext(),ProductInsertActivity.class);
                    startActivity(goinsertIntent);
                }
            });

        }
//        showlistId.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//                ShopModel shopModel=productslist.get(i);
//                Intent singleIntent=new Intent(ShowProductActivity.this,ProductDetailsActivity.class);
//                singleIntent.putExtra("product", (Serializable) shopModel);
//                //singleIntent.putExtra("id",productslist.get(i).getId());
//                startActivity(singleIntent);
//            }
//        });

//        showlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(getApplicationContext(),productslist.get(position).toString(),Toast.LENGTH_SHORT).show();
//            }
//        });


    }
//    public void goInsert(View view) {
//        Intent insertIntent=new Intent(getApplicationContext(),ProductInsertActivity.class);
//        startActivity(insertIntent);
//    }
}
