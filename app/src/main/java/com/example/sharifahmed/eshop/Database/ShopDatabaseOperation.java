package com.example.sharifahmed.eshop.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;

import com.example.sharifahmed.eshop.Model.ShopModel;

import java.util.ArrayList;


public class ShopDatabaseOperation {

    ShopDatabaseHelper shopDatabaseHelper;
    SQLiteDatabase database;
    ShopModel shopModel;

    public ShopDatabaseOperation(Context context) {
        shopDatabaseHelper=new ShopDatabaseHelper(context);
    }
    public void open(){
        database=shopDatabaseHelper.getWritableDatabase();
    }
    public void close(){
        shopDatabaseHelper.close();
    }
    public boolean addPoduct(ShopModel shopModel){
        this.open();

        ContentValues contentValues=new ContentValues();
        contentValues.put(ShopDatabaseHelper.COL_PRODUCT_NAME,shopModel.getProductName());
        contentValues.put(ShopDatabaseHelper.COL_PRODUCT_PRICE,shopModel.getProductPrice());
        contentValues.put(ShopDatabaseHelper.COL_PRODUCT_IMAGE,shopModel.getProductImage());
        //contentValues.put(ShopDatabaseHelper.COL_PRODUCT_QUANTITY,shopModel.getProductQuantity());
        contentValues.put(ShopDatabaseHelper.COL_PRODUCT_DESCRIPTION,shopModel.getProductDetails());


        long inserted=database.insert(ShopDatabaseHelper.TABLE_PRODUCT,null,contentValues);

        this.close();
        if(inserted>0){
            return true;
        }else {
            return false;
        }

    }

    public boolean updateProduct(int id,ShopModel shopModel){
        this.open();

        ContentValues contentValues=new ContentValues();

        contentValues.put(ShopDatabaseHelper.COL_PRODUCT_NAME,shopModel.getProductName());
        contentValues.put(ShopDatabaseHelper.COL_PRODUCT_PRICE,shopModel.getProductPrice());
        contentValues.put(ShopDatabaseHelper.COL_PRODUCT_IMAGE,shopModel.getProductImage());
        //contentValues.put(ShopDatabaseHelper.COL_PRODUCT_QUANTITY,shopModel.getProductQuantity());
        contentValues.put(ShopDatabaseHelper.COL_PRODUCT_DESCRIPTION,shopModel.getProductDetails());

        int updated=database.update(ShopDatabaseHelper.TABLE_PRODUCT,contentValues,ShopDatabaseHelper.COL_PRODUCT_ID+" = "+id,null);
        this.close();

        if(updated>0){
            return true;
        }else{
            return false;
        }

    }

    public boolean deleteProduct(int id){
        this.open();

        int deleted=database.delete(ShopDatabaseHelper.TABLE_PRODUCT,ShopDatabaseHelper.COL_PRODUCT_ID+" = "+id,null);

        this.close();
        if(deleted>0){
            return true;
        }
        else {
            return false;
        }
    }


    public ArrayList<ShopModel>getAllProduct(){
        ArrayList<ShopModel>shopModels=new ArrayList<>();
        this.open();

        Cursor cursor=database.rawQuery("select * from "+ShopDatabaseHelper.TABLE_PRODUCT,null);

        if(cursor!=null && cursor.getCount()>0){
            cursor.moveToFirst();
            for(int i=0;i<cursor.getCount();i++){

                int mProductId=cursor.getInt(cursor.getColumnIndex(ShopDatabaseHelper.COL_PRODUCT_ID));
                String mProductName=cursor.getString(cursor.getColumnIndex(ShopDatabaseHelper.COL_PRODUCT_NAME));
                double mProductPrice=cursor.getDouble(cursor.getColumnIndex(ShopDatabaseHelper.COL_PRODUCT_PRICE));
                String mProductImage=cursor.getString(cursor.getColumnIndex(ShopDatabaseHelper.COL_PRODUCT_IMAGE));
                //int mProductQuantity=cursor.getInt(cursor.getColumnIndex(ShopDatabaseHelper.COL_PRODUCT_QUANTITY));
                String mProductDescription=cursor.getString(cursor.getColumnIndex(ShopDatabaseHelper.COL_PRODUCT_DESCRIPTION));

                shopModel=new ShopModel(mProductId,mProductName,mProductPrice,mProductImage,mProductDescription);
                cursor.moveToNext();
                shopModels.add(shopModel);
            }
        }
        cursor.close();
        this.close();
        return shopModels;
    }

    public ShopModel getSingleProductById(int id){
        this.open();

        Cursor cursor=database.query(ShopDatabaseHelper.TABLE_PRODUCT,new String[]{ShopDatabaseHelper.COL_PRODUCT_NAME,ShopDatabaseHelper.COL_PRODUCT_PRICE,ShopDatabaseHelper.COL_PRODUCT_IMAGE,ShopDatabaseHelper.COL_PRODUCT_DESCRIPTION},ShopDatabaseHelper.COL_PRODUCT_ID+" = "+id,null,null,null,null);

        cursor.moveToFirst();

        //int mProductId=cursor.getInt(cursor.getColumnIndex(ShopDatabaseHelper.COL_PRODUCT_ID));
        String mProductName=cursor.getString(cursor.getColumnIndex(ShopDatabaseHelper.COL_PRODUCT_NAME));
        double mProductPrice=cursor.getDouble(cursor.getColumnIndex(ShopDatabaseHelper.COL_PRODUCT_PRICE));
        String mProductImage=cursor.getString(cursor.getColumnIndex(ShopDatabaseHelper.COL_PRODUCT_IMAGE));
        //int mProductQuantity=cursor.getInt(cursor.getColumnIndex(ShopDatabaseHelper.COL_PRODUCT_QUANTITY));
        String mProductDescription=cursor.getString(cursor.getColumnIndex(ShopDatabaseHelper.COL_PRODUCT_DESCRIPTION));

        cursor.close();
        this.close();

        shopModel=new ShopModel(mProductName,mProductPrice,mProductImage,mProductDescription);
        return shopModel;
    }
}
