package com.example.sharifahmed.eshop.Activity;

import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sharifahmed.eshop.Database.ShopDatabaseOperation;
import com.example.sharifahmed.eshop.Model.ShopModel;
import com.example.sharifahmed.eshop.R;

import java.util.ArrayList;

public class ProductInsertActivity extends AppCompatActivity {

    EditText productNameET;
    EditText productPriceET;
    ImageView productImageView;
    EditText productDescriptionET;
    EditText productQuantityET;
    TextView productImagePath;

    Button uploadPhoto;
    Button saveBtn;
    Button showBtn;

    String productName;
    double productPrice;
    String productImage;
    int productQuantity;
    String productDescription;
    boolean statusUpdate,statusInsert;
    ShopModel productByName;
    int id;

    ShopDatabaseOperation shopDatabaseOperation;
    SQLiteDatabase database;
    ShopModel shopModel;


    private static int RESULT_LOAD_IMAGE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_insert);


        shopDatabaseOperation=new ShopDatabaseOperation(this);
        saveBtn = (Button) findViewById(R.id.productSaveId);
        uploadPhoto = (Button) findViewById(R.id.load_image);


        productNameET= (EditText) findViewById(R.id.productNameId);
        productPriceET= (EditText) findViewById(R.id.productPriceId);
        productImageView= (ImageView) findViewById(R.id.productImageId);
        productImagePath = (TextView) findViewById(R.id.itemsImageAddText);
        productDescriptionET= (EditText) findViewById(R.id.productDescriptionId);


        id=getIntent().getIntExtra("id",0);

        if(id>0){
            try{
                shopModel=shopDatabaseOperation.getSingleProductById(id);
                productNameET.setText(shopModel.getProductName());
                productPriceET.setText(String.valueOf(shopModel.getProductPrice()));

                productImagePath.setText(shopModel.getProductImage());
                productImageView.setImageDrawable(Drawable.createFromPath(shopModel.getProductImage()));

                productDescriptionET.setText(shopModel.getProductDetails());
                saveBtn.setText("update");
            }
            catch (CursorIndexOutOfBoundsException ex){
                Toast.makeText(getApplicationContext(),"No Data You Want To Update",Toast.LENGTH_SHORT).show();
                Intent goInsertIntent = new Intent(getApplicationContext(),ProductInsertActivity.class);
                startActivity(goInsertIntent);
            }

        }

        uploadPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent openImage = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(openImage, RESULT_LOAD_IMAGE);
            }
        });

    }





    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String imagePath=getRealPathFromURI(selectedImage);
            Drawable d = Drawable.createFromPath(imagePath);
            productImageView.setImageDrawable(d);
            productImagePath.setText(imagePath);
        }
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }










    public void SaveProduct(View view) {

        productName = productNameET.getText().toString().trim();
        if(productName.length()<=0){
            productNameET.setError("Enter Product Name");
        }

        Double price;
        try {
            price = new Double(productPriceET.getText().toString());
        } catch (NumberFormatException e) {
            price = 0.0;
        }

        productPrice = price;
        if(productPrice<=0.0){
            productPriceET.setError("Enter Valid Product Price");
        }

//        int img;
//        try {
//            img = new Integer(productImageET.getText().toString());
//        } catch (NumberFormatException e) {
//            img = 0;
//        }

//        productImage = img;
//        if(productImage<=0){
//            productImageET.setError("Enter Product Image");
//        }

//        int qty;
//        try {
//            qty = new Integer(productQuantityET.getText().toString());
//        } catch (NumberFormatException e) {
//            qty = 0;
//        }
//
//        productQuantity = qty;
//        if(productQuantity<=0){
//            productQuantityET.setError("Enter Product Quantity");
//        }


        productImage = productImagePath.getText().toString();

        productDescription = productDescriptionET.getText().toString().trim();
        if(productDescription.length()<=0){
            productDescriptionET.setError("Enter Product Description");
        }




        if(productName.length()==0 || productPrice<=0.0 || productDescription.length()==0){

            Toast.makeText(ProductInsertActivity.this, "Please Fill the Valid Input data", Toast.LENGTH_SHORT).show();
        }

        else
        {
            shopModel = new ShopModel(productName,productPrice,productImage,productDescription);

//            productByName=shopDatabaseOperation.getSingleProductByName(shopModel.getProductName());
//            int st2= shopModel.getProductName().compareTo(productByName.getProductName());
//            if(st2==1){
//                Toast.makeText(ProductInsertActivity.this,"Already Have The Same Name product", Toast.LENGTH_SHORT).show();
//            }
            if(id>0){
                statusUpdate=shopDatabaseOperation.updateProduct(id,shopModel);
                if(statusUpdate) {
                    Toast.makeText(ProductInsertActivity.this,"Product Update Successful", Toast.LENGTH_SHORT).show();
                }
            }else {
                statusInsert=shopDatabaseOperation.addPoduct(shopModel);
                if(statusInsert) {
                    Toast.makeText(ProductInsertActivity.this,"Product Insert Successful", Toast.LENGTH_SHORT).show();
                    productNameET.setText("");
                    productPriceET.setText("");
                    productDescriptionET.setText("");
                }
            }
        }

    }

    public void ShowProdut(View view) {
        Intent goShowIntent = new Intent(getApplicationContext(),ShowProductActivity.class);
        startActivity(goShowIntent);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main,menu);
        return true;
    }

    public void homeClicked(MenuItem menu){

        Toast.makeText(this,"Clicked Home",Toast.LENGTH_SHORT).show();
    }
}
