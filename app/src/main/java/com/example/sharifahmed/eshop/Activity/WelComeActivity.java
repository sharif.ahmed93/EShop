package com.example.sharifahmed.eshop.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.sharifahmed.eshop.R;

public class WelComeActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
    }

    public void goInsert(View view) {
        Intent insertIntent=new Intent(getApplicationContext(),ProductInsertActivity.class);
        startActivity(insertIntent);
    }
}
