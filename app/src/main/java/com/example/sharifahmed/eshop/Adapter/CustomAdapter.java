package com.example.sharifahmed.eshop.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sharifahmed.eshop.Activity.ProductDetailsActivity;
import com.example.sharifahmed.eshop.Model.ShopModel;
import com.example.sharifahmed.eshop.R;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter<ShopModel> {

    private Context context;
    ArrayList<ShopModel> products;



    public CustomAdapter(Context context, ArrayList<ShopModel> products) {
        super(context, R.layout.custom_show_product_list,products);
        this.context=context;
        this.products=products;
    }

    private final static class  ViewHolder{
        ImageView productImage;
        TextView productName;
        TextView productPrice;
        LinearLayout clickProduct;


    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if(convertView==null){

            viewHolder=new ViewHolder();

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView =layoutInflater.inflate(R.layout.custom_show_product_list,null);

            viewHolder.clickProduct = (LinearLayout) convertView.findViewById(R.id.clickedId);

            viewHolder.productImage = (ImageView) convertView.findViewById(R.id.customShowImageId);

            viewHolder.productName = (TextView) convertView.findViewById(R.id.customShowNameId);
            viewHolder.productPrice = (TextView) convertView.findViewById(R.id.customShowPriceId);



            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.clickProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goProductDetailsIntent = new Intent(context.getApplicationContext(),ProductDetailsActivity.class);
                goProductDetailsIntent.putExtra("id",products.get(position).getId());
                context.startActivity(goProductDetailsIntent);
            }
        });

        String imagePath = products.get(position).getProductImage();
        //String imagePath = items.getItemImage();
        try{
            Drawable d = Drawable.createFromPath(imagePath);
            viewHolder.productImage.setImageDrawable(d);
        }

        catch(OutOfMemoryError e){
            Toast.makeText(context.getApplicationContext(),"Large Size Image Inserted",Toast.LENGTH_SHORT).show();
        }
        viewHolder.productName.setText(products.get(position).getProductName());
        viewHolder.productPrice.setText(String.valueOf(products.get(position).getProductPrice()));



        return convertView;
    }
}
