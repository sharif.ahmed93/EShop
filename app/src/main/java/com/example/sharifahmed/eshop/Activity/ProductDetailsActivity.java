package com.example.sharifahmed.eshop.Activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sharifahmed.eshop.Database.ShopDatabaseOperation;
import com.example.sharifahmed.eshop.Model.ShopModel;
import com.example.sharifahmed.eshop.R;

import java.util.ArrayList;

public class ProductDetailsActivity extends AppCompatActivity {

    int id;

    ImageView productImage;
    TextView productNameText;
    TextView productPriceText;
    TextView productDescriptionText;

    ShopDatabaseOperation shopDatabaseOperation;
    ShopModel shopModel;
    ShopModel shopModelFromShow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        productImage =(ImageView) findViewById(R.id.imageDetailsId);
        productNameText= (TextView) findViewById(R.id.detailsNameId);
        productPriceText= (TextView) findViewById(R.id.detailsPriceId);
        productDescriptionText= (TextView) findViewById(R.id.detailsProductDescriptionId);



        shopModelFromShow = (ShopModel) getIntent().getSerializableExtra("product");

        //id = shopModelFromShow.getId();

        id=getIntent().getIntExtra("id",0);

        shopDatabaseOperation=new ShopDatabaseOperation(this);
        shopModel=shopDatabaseOperation.getSingleProductById(id);

        String imagePath = shopModel.getProductImage();
        Drawable d = Drawable.createFromPath(imagePath);

        productImage.setImageDrawable(d);
        productNameText.setText(shopModel.getProductName());
        productPriceText.setText(String.valueOf(shopModel.getProductPrice()));
        productDescriptionText.setText(shopModel.getProductDetails());

    }



    public void UpdateProduct(View view) {
        if(id!=0){
            Intent updateIntent=new Intent(ProductDetailsActivity.this,ProductInsertActivity.class);
            updateIntent.putExtra("id",id);
            startActivity(updateIntent);
        }



    }

    public void DeleteProduct(View view) {
            shopDatabaseOperation.deleteProduct(id);
            Intent goShowProductIntent=new Intent(getApplicationContext(),ShowProductActivity.class);
            startActivity(goShowProductIntent);
    }
}
