package com.example.sharifahmed.eshop.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sharifahmed.eshop.Model.Preference;

import com.example.sharifahmed.eshop.R;
    public class LoginActivity extends AppCompatActivity {

        EditText userNameText;
        EditText passwordText;
        Button loginButton;
        Preference preferenc;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            preferenc=new Preference(LoginActivity.this);
            if(preferenc.getLoginStatus()==true)
            {
                Intent welComeInten=new Intent(this,WelComeActivity.class);
                startActivity(welComeInten);
            }
            setContentView(R.layout.activity_login);
            this.getInitialize();
        }

        public void getInitialize()
        {
            userNameText= (EditText) findViewById(R.id.user_name);
            passwordText= (EditText) findViewById(R.id.password);
            loginButton= (Button) findViewById(R.id.login_button);
            loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String userName=userNameText.getText().toString();
                    String userPassword=passwordText.getText().toString();
                    if(userName.length()<=0)
                    {
                        Toast.makeText(LoginActivity.this, "Please Enter User Name", Toast.LENGTH_SHORT).show();
                    }
                    else if(userPassword.length()<=0)
                    {
                        Toast.makeText(LoginActivity.this, "Please Enter Password", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        preferenc.saveUserData(userName,userPassword);
                        Intent welComeInten=new Intent(getApplicationContext(),WelComeActivity.class);
                        startActivity(welComeInten);
                    }
                }
            });
        }
    }
