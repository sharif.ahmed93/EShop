package com.example.sharifahmed.eshop.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class ShopDatabaseHelper extends SQLiteOpenHelper {

    static final String DATABASE_NAME="product_manager";
    static final int DATABASE_VERSION=1;

    static final String TABLE_PRODUCT="product_info";

    static final String COL_PRODUCT_ID="id";
    static final String COL_PRODUCT_NAME="product_name";
    static final String COL_PRODUCT_PRICE="product_price";
    static final String COL_PRODUCT_IMAGE="product_image";
    //static final String COL_PRODUCT_QUANTITY="product_quantity";
    static final String COL_PRODUCT_DESCRIPTION="product_description";


   // static final String CREATE_TABLE_PRODUCT="create table "+TABLE_PRODUCT+"( "+COL_PRODUCT_ID+" integer primary key, "+COL_PRODUCT_NAME+" text, "+COL_PRODUCT_PRICE+" text, "+COL_PRODUCT_IMAGE+" text, "+COL_PRODUCT_QUANTITY+" text, "+COL_PRODUCT_DESCRIPTION+" text);";


    static final String CREATE_TABLE_PRODUCT="create table "+TABLE_PRODUCT+"( "+COL_PRODUCT_ID+" integer primary key, "+COL_PRODUCT_NAME+" text, "+COL_PRODUCT_PRICE+" text, "+COL_PRODUCT_IMAGE+" text, "+COL_PRODUCT_DESCRIPTION+" text);";

    public ShopDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(CREATE_TABLE_PRODUCT);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

        sqLiteDatabase.execSQL("drop table if exist"+TABLE_PRODUCT);
        onCreate(sqLiteDatabase);

    }
}
