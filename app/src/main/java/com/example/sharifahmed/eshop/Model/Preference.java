package com.example.sharifahmed.eshop.Model;

import android.content.Context;
import android.content.SharedPreferences;


public class Preference {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Context context;
    private static final String PREF_NAME = "userData";
    private static final String NAME_KEY = "userName";
    private static final String PASSWORD_KEY = "userPassword";

    public Preference(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void saveUserData(String userName, String userPassword) {
        editor.putString(NAME_KEY, userName);
        editor.putString(PASSWORD_KEY, userPassword);
        editor.commit();
    }

    public boolean getLoginStatus() {
        String userName = sharedPreferences.getString(NAME_KEY,null);
        String userPassword = sharedPreferences.getString(PASSWORD_KEY,null);
        if(userName!=null && userPassword!=null)
            return true;
        else
            return false;
    }
}
