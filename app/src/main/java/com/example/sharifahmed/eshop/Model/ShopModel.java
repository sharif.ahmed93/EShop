package com.example.sharifahmed.eshop.Model;


public class ShopModel {

    private int id;
    private String productName;
    private double productPrice;
    private String productImage;
    //private int productQuantity;
    private String productDetails;


    public ShopModel(int id, String productName, double productPrice, String productImage ,String productDetails) {
        this.id = id;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productImage = productImage;
        //this.productQuantity = productQuantity;
        this.productDetails = productDetails;
    }

    public ShopModel(String productName, double productPrice, String productImage, String productDetails) {
        this.productName = productName;
        this.productPrice = productPrice;
        this.productImage = productImage;
        //this.productQuantity = productQuantity;
        this.productDetails = productDetails;
    }

    public ShopModel() {
    }

    public int getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public String getProductImage() {
        return productImage;
    }

//    public int getProductQuantity() {
//        return productQuantity;
//    }

    public String getProductDetails() {
        return productDetails;
    }
}
